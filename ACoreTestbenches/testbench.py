import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))
from thesdk import *
from acore_methods import acore_methods

class TestBench(thesdk):
    """
    Holds properties common to all A-Core test benches.

    Concrete test bench implementations should inherit this class.
    """

    def __init__(self, **kwargs):
        pass

#region properties

    @property
    def test_target(self):
        """
        Name of the test target to be simulated. The loaded configuration corresponding
        to the given name is stored in the ``test_config`` property.
        """
        if not hasattr(self, '_test_target'):
            self._test_target = None
        return self._test_target
    @test_target.setter
    def test_target(self, value):
        self._test_target = value

    @property
    def test_config(self):
        """
        Test configuration dictionary that represents a single well-defined test.
        """
        if not hasattr(self, '_test_config'):
            self._test_config = None
        return self._test_config
    @test_config.setter
    def test_config(self, value):
        self._test_config = value

#endregion properties

