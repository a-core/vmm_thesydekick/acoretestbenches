# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, Edge, Timer
import numpy as np
import common

TIMEOUT = 100000

# NOTE: icarus supports only 'step's as time unit, therefore we should use only them

# Timeout is 2x'd because a clock cycle is 2 steps
@cocotb.test(timeout_time=2*TIMEOUT, timeout_unit="step")
async def test_ACoreChip(dut):
    """
    This test loads the program from an elf file into ROM, toggles reset, and runs the clock
    for a specified number of clock cycles
    """
    
    async def jtag_cycle(tck, tms, tdi, trstn):
        """
        Execute one jtag cycle.
        """
        try:
            dut.io_jtag_TMS.value = tms
            dut.io_jtag_TDI.value = tdi
            dut.io_jtag_TCK.value = tck
            dut.io_jtag_TRSTn.value = trstn
            await ClockCycles(clock, 1)
        except Exception as e:
            print(e)

    elf_path = cocotb.plusargs["elf"]
    selfchecking = cocotb.plusargs["selfchecking"] == "True"
    jtag_file = cocotb.plusargs["jtag_file"]
    if not elf_path:
        raise Exception("Please provide an elf file. Example: make ELF_FILE=/path/to/elf.elf")
    elf_file = common.read_elf(elf_path)

    clock = dut.clock
    reset = dut.reset

    # Dummy values to input IOs
    dut.io_gpi.value = 0x55

    # Clock is 2 steps per cycle because icarus does not support 0.5 cycles
    cocotb.start_soon(Clock(clock, 2, units="step").start())
    await jtag_cycle(0, 0, 0, 0)

    jtag_array = np.loadtxt(jtag_file)
    
    # Initialize ROM
    rom_blob = common.rom_image(elf_file)
    rom_blocks = [
        dut.progmem.mem_array_0,
        dut.progmem.mem_array_1,
        dut.progmem.mem_array_2,
        dut.progmem.mem_array_3
        ]
    for i in range(len(rom_blob)):
        rom_blocks[i%len(rom_blocks)][i//len(rom_blocks)].value = int(rom_blob[i])

    reset.value = 0

    await ClockCycles(clock, 2)
    reset.value = 1
    await ClockCycles(clock, 2)
    reset.value = 0
    await ClockCycles(clock, 2)

    for item in jtag_array:
        await jtag_cycle(int(item[0]), int(item[1]), int(item[2]), int(item[3]))

    if selfchecking:
        await Edge(dut.core.csreg_block.csregs.mstopsim)
        assert dut.core.regfile_block.regfile.x_10 == 1
    else:
        await Timer(TIMEOUT-10)
