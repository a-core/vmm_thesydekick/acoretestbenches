"""
ACoreTestbenches
================

Container entity for different A-Core test bench configurations. A test
bench is constructed by instantiating ``ACoreTestbenches`` and calling
the appropriate testbench setup method.

Example
-------

::

    from ACoreTestbenches import *
    testbench=ACoreTestbenches()
    testbench.define_default_testbench()
    testbench.run()

"""

from asyncio import subprocess
import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from ACoreChip import *
from ACoreChip.controller import controller as ACoreChip_controller
from ACoreChip.controller import jtag_controller
from JTAG.driver import jtag_driver
from jtag_programmer import *
from chisel_methods import chisel_methods
import acore_methods.pathutils as pathutils
import cocotb_test.simulator
import os
import subprocess
import pathlib
import glob
import struct
import socket

from .testbench import TestBench
from .sim_testbench import SimTestbench
from .zc706_testbench import ZC706Testbench

import numpy as np
from enum import Enum

class SelectedTestBench(Enum):
    UNDEFINED = 0
    SIM_ACORECHIP = 1
    SIM_RISCV_TESTS = 2
    IMPL_ZC706_ACORECHIP = 3

class ACoreTestbenches(thesdk):
    """
    Testbenches for A-Core Tests

    Parameters
    ----------
    questa_wave_fmt : str
        Format in which Questa Simulator exports a waveform. For non-interactive tests.
        'wlf' (default) or 'vcd'
    """
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self,*arg, questa_wave_fmt='none', **kwargs): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        self.proplist = [ 'Rs' ]; # Properties that can be propagated from parent
        self.Rs =  100e6;         # Sampling frequency
        self.model = 'py';        # Can be set externally, but is not propagated
        self.par = False          # By default, no parallel processing
        self.queue = []           # By default, no parallel processing
    

        if len(arg) >= 1:
            parent = arg[0]
            self.copy_propval(parent, self.proplist)
            self.parent = parent

        self.questa_wave_fmt = questa_wave_fmt
        self.backend = 'questa'

        self.init()

    def init(self):
        self.testbench = SelectedTestBench.UNDEFINED
        self.sim = 'thesdk'


    def check_sim_result(self):
        try:
            # Take the last value
            result = self.dut.IOS.Members['a0'].Data[-1]
        except TypeError: # Raised if there is no data in a0
            self.print_log(type='I', msg="Test failed! There was no data in a0.")
            raise AssertionError("Failed test")

        if result != '1':
            self.print_log(type='I', msg="Test failed! a0 was not 1.")
            raise AssertionError("Failed test")
        else:
            self.print_log(type='I', msg="Test passed!")


    def mstopsim_init(self):
        # Connectors to store a0 (x10) to an iofile (the iofile is defined under run-command)
        # Also generates mstopsim wire that connects to mstopsim CSR
        self.dut.custom_connectors = verilog_connector_bundle()
        self.dut.custom_connectors.Members['a0'] = verilog_connector(name='a0', cls='wire')
        self.dut.custom_connectors.Members['mstopsim'] = verilog_connector(name='mstopsim', cls='wire')
        self.dut.custom_connectors.Members['ACoreChip.core.csreg_block.csregs.mstopsim'] = verilog_connector(name='ACoreChip.core.csreg_block.csregs.mstopsim')
        self.dut.custom_connectors.Members['ACoreChip.core.regfile_block.regfile.x_10'] = verilog_connector(name='ACoreChip.core.regfile_block.regfile.x_10')
        self.dut.custom_connectors.Members['mstopsim'].connect = self.dut.custom_connectors.Members['ACoreChip.core.csreg_block.csregs.mstopsim']
        self.dut.custom_connectors.Members['a0'].connect = self.dut.custom_connectors.Members['ACoreChip.core.regfile_block.regfile.x_10']
        self.dut.assignment_matchlist = ['mstopsim', 'a0']


        # Makes simulation stop when test program writes in mstopsim register
        stop_early_cmd = """
reg mstopsim_delayed;
always @(posedge clock) begin
    mstopsim_delayed <= mstopsim;
end
always @(mstopsim_delayed) begin
    if (mstopsim_delayed == 1) begin
        $finish;
    end
end
            """
        self.dut.rtlmisc = [stop_early_cmd]

        # Saves a0 to an iofile when mstopsim is 1
        self.dut.IOS.Members['a0'] = IO()
        a0 = rtl_iofile(self.dut, name='a0', dir='out', iotype='sample', ionames=['a0'], datatype='sint')
        a0.verilog_io_condition = 'mstopsim == 1'


    # riscv-tests specific setup
    def define_riscv_tests_testbench(                                    
                                    self, 
                                    default_isa_string=None, 
                                    program_file=None, 
                                    jtag_config_file=None,
                                    isa_string=None,
                                    interactive_controlfile=None,
                                    simulator=None,
                                    backend=None,
                                    **kwargs):
        """
        Configures this testbench instance for riscv-tests.

        Parameters
        ----------

        jtag_config_file : str
            Path to JTAG configuraiton yaml file.

        isa_string : str
            Base and extensions to be generated. default ``"rv32imf"``.
        """
        # mark defined testbench
        self.testbench = SelectedTestBench.SIM_RISCV_TESTS

        # use the cocotb by default
        self.sim = simulator

        self.isa_string = isa_string if isa_string != "default" else default_isa_string

        # Use thesdk as the simulator
        if self.sim == 'thesdk':
            # parse keyword arguments
            self.program_file = None
            self.jtag_config_file = jtag_config_file
            self.interactive_controlfile = interactive_controlfile

            # Original DUT- used only for Verilog generation
            self.original_dut = ACoreChip()
            self.original_dut.vlogext = '.v'
            
            # Generate Verilog for the DUT
            self.chisel = chisel_methods()
            self.chisel.run_chisel_generator(
                entity=self.original_dut,
                mainclass='acorechip.ACoreChip',
                args={
                    '-isa_string': self.isa_string,
                    '-jtag_config': self.jtag_config_file,
                    '-debug': 'true'
                }
            )

            # Move Verilog files to standard location: ACoreChip/sv (vlogsrcpath)

            # Create folder for source files
            if not os.path.exists(self.original_dut.vlogsrcpath):
                os.mkdir(self.original_dut.vlogsrcpath)
            # Remove old files if any
            [os.remove(file) for file in glob.glob(os.path.join(self.original_dut.vlogsrcpath, '*'))]
            # Move files
            for file in glob.glob(os.path.join(self.original_dut.rtlsimpath, '*')):
                os.rename(file, os.path.join(self.original_dut.vlogsrcpath, os.path.basename(file)))


        # use cocotb as the simulator
        if self.sim == 'cocotb':
            self.print_log(type='I', msg="Using simulator 'cocotb' for riscv-tests")

            # parse keyword arguments
            self.program_file = None
            self.jtag_config_file = jtag_config_file

            # generate dut verilog
            self.dut = ACoreChip()
            self.dut.vlogext = '.v'
            self.dut.vlogmodulefiles = ['AsyncResetSyncBB.v']

            self._hw_config_name = pathutils.barename(kwargs['test_config']['hw_config_yaml'])
            self._acoretests_build_dir = os.path.realpath(os.path.join(self.entitypath, '../ACoreTests/build/'))

            # Copy verilog files from 'build/chisel/' to rtl simulation directory
            verilog_path = os.path.join(self._acoretests_build_dir, 'chisel', self._hw_config_name)
            verilog_sources = glob.glob(os.path.join(verilog_path, '*.v'))
            for vsrc in verilog_sources:
                dstfile = os.path.join(self.dut.rtlsimpath, os.path.basename(vsrc))
                shutil.copyfile(vsrc, dstfile)

            self.interactive_sim = False
            if kwargs['interactive_sim']:
                # "cocotbify" the verilog - allows generating VCD trace of the simulation
                self.interactive_sim = True
                cocotbify_cmd = 'cocotbify -v ' + self.dut.simdut + ' -o ' + self.dut.simdut
                self.print_log(type='I', msg="Running command: %s" % cocotbify_cmd)
                output = subprocess.check_output(cocotbify_cmd, shell=True)
                print(output.decode('utf-8'))

    def set_riscv_tests_executable(self, program_file):
        if self.testbench is not SelectedTestBench.SIM_RISCV_TESTS:
            self.print_log(type='F', msg='set_riscv_tests_executable(): define riscv-tests testbench first with `define_riscv_tests_testbench()`')
        self.program_file = program_file

    def run_riscv_tests_testbench(self):

        if self.sim == 'cocotb':
            # use icarus verilog for now
            os.environ["SIM"] = 'icarus'

            if self.program_file is None:
                self.print_log(type='F', msg='run_riscv_tests_testbench(): no test selected, select with `set_riscv_tests_executable()`')

            # determine test specific paths
            script_path = str(pathlib.Path(__file__).parent)
            sim_build = os.path.join('sim_build', os.path.basename(self.program_file))
            work_dir = os.path.join(script_path, sim_build)

            # change working directory to write vcd dumps to `work_dir`
            pathlib.Path(work_dir).mkdir(parents=True, exist_ok=True)
            os.chdir(work_dir)

            # collect all verilog sources
            verilog_sources = [self.dut.simdut] + \
                [os.path.join(self.dut.rtlsimpath, file) for file in self.dut.vlogmodulefiles]

            # run cocotb simulation
            result = cocotb_test.simulator.run(
                verilog_sources=verilog_sources,
                work_dir=work_dir,
                sim_build=sim_build,
                python_search=[script_path],
                toplevel="ACoreChip",
                module="test_riscv_tests",
                plus_args=["+elf=" + self.program_file],
            )

            if self.interactive_sim:
                self.print_log(type='I', msg="Dumping VCD at : %s" % work_dir)

        elif self.sim == 'thesdk':
            # determine test specific paths
            script_path = str(pathlib.Path(__file__).parent)
            sim_build = os.path.join('sim_build_thesdk', os.path.basename(self.program_file))
            work_dir = os.path.join(script_path, sim_build)
            # Remove old files if any
            [os.remove(file) for file in glob.glob(os.path.join(work_dir, '*.txt'))]

            # change working directory to write vcd dumps to `work_dir`
            pathlib.Path(work_dir).mkdir(parents=True, exist_ok=True)
            os.chdir(work_dir)

            # setup dut
            self.dut = ACoreChip()
            self.fd = open(self.jtag_config_file, 'r')
            self.jtag_config = yaml.load(self.fd, yaml.Loader)
            self.fd.close()
            self.dut.vlogext = '.v'
            self.dut.vlogmodulefiles = ['AsyncResetSyncBB.v']
            self.dut.model = 'sv'
            self.dut.Rs = self.Rs
            self.dut.preserve_iofiles = False
            self.dut.preserve_rtlfiles = False
            self.dut.interactive_rtl = False
            self.dut.interactive_controlfile = \
                os.path.join(self.dut.entitypath, "interactive_control_files/modelsim/log_all_to_{}.do".format(self.questa_wave_fmt))
            self.dut.vlogsimargs = ["-quiet"]
            self.dut.vlogcompargs = ["-quiet"]

            self.mstopsim_init()

            self.driver = jtag_driver()
            self.namemap = {
                'address':  'prog_iface_addr',
                'data':     'prog_iface_data',
                'write_en': 'prog_iface_write_en'
            }

            self.p = jtag_programmer(
                driver=self.driver, config=self.jtag_config, namemap=self.namemap)

            # simulation sequencing
            
            self.controller = ACoreChip_controller(dut=self.dut, file=self.dut.vlogsrc)
            self.controller.Rs = self.Rs  # This can be propagatet with proplist attribute

            # Connect IOs
            self.dut.IOS.Members['control_write'] = self.controller.IOS.Members['control_write']

            self.jtag_ctrl = jtag_controller(self.driver, self.jtag_config)
            self.driver.reset_tap()
            self.jtag_ctrl.set_core_en(0)
            self.p.read_elf(self.program_file)
            self.p.run()
            self.jtag_ctrl.set_core_en(1)
            self.controller.reset()
            self.controller.step_time()

            self.controller.start_datafeed()
            self.controller.step_time(step=len(self.p.wave)*self.controller.step)
            self.controller.step_time(step=500000*self.controller.step)
            self.controller.set_simdone()

            # convert jtag IO to numpy array
            self.dut.IOS.Members['jtag_tap_in'].Data = np.array(
                self.driver.IOS.Members['jtag_tap_in'].Data, dtype=np.ubyte)

            self.dut.init()
            self.dut.run()

            self.check_sim_result()


    def run(self):
        """ Default entity runner. Runs the defined testbench.
        """
        if self.model == 'py':
            if self.testbench == SelectedTestBench.SIM_RISCV_TESTS:
                self.run_riscv_tests_testbench()
            else:
                self.print_log(type='F', msg='Undefined testbench!')
