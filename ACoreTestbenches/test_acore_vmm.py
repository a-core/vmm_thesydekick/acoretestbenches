# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, Edge, RisingEdge
from cocotb.queue import Queue
import numpy as np
import common

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))
if not (os.path.abspath('../../vmm_top') in sys.path):
    sys.path.append(os.path.abspath('../../vmm_top'))

from thesdk import *
from vmm_top import *

"""
VMM IOS:

INPUTS:
vmm_top.IOS.Members['acore_clk']
vmm_top.IOS.Members['addr']
vmm_top.IOS.Members['d_in']
vmm_top.IOS.Members['sram_wr']
vmm_top.IOS.Members['sram_rd']

OUTPUTS:
vmm_top.IOS.Members['adc_data_ready']
vmm_top.IOS.Members['adc_00']
vmm_top.IOS.Members['adc_01']
vmm_top.IOS.Members['adc_02']
vmm_top.IOS.Members['adc_03']
vmm_top.IOS.Members['adc_04']
vmm_top.IOS.Members['adc_05']
vmm_top.IOS.Members['adc_06']
vmm_top.IOS.Members['adc_07']
vmm_top.IOS.Members['adc_08']
vmm_top.IOS.Members['adc_09']
vmm_top.IOS.Members['adc_10']
vmm_top.IOS.Members['adc_11']
vmm_top.IOS.Members['adc_12']
vmm_top.IOS.Members['adc_13']
vmm_top.IOS.Members['adc_14']
vmm_top.IOS.Members['adc_15']
vmm_top.IOS.Members['adc_16']
vmm_top.IOS.Members['adc_17']
vmm_top.IOS.Members['adc_18']
vmm_top.IOS.Members['adc_19']
vmm_top.IOS.Members['adc_20']
vmm_top.IOS.Members['adc_21']
vmm_top.IOS.Members['adc_22']
vmm_top.IOS.Members['adc_23']
vmm_top.IOS.Members['adc_24']
vmm_top.IOS.Members['adc_25']
vmm_top.IOS.Members['adc_26']
vmm_top.IOS.Members['adc_27']
vmm_top.IOS.Members['adc_28']
vmm_top.IOS.Members['adc_29']
vmm_top.IOS.Members['adc_30']
vmm_top.IOS.Members['adc_31']
"""

class vmm_wrapper:
    """
    Cocotb wrapper for vmm SyDeKick Entity
    """
    def __init__(self, in_queue: Queue = None, out_queue: Queue = None):
        self.in_queue = in_queue
        self.out_queue = out_queue
        self.vmm = vmmObject()
        cocotb.start_soon(self.run())

    async def run(self):
        while True:
            new_inputs = await self.in_queue.get()
            self.vmm.IOS.Members.update(new_inputs.Members)
            self.vmm.run()
            await self.out_queue.put(self.vmm.IOS.Members)


async def connect_vmm(dut, clock):
    vmm_in_queue = Queue()
    vmm_out_queue = Queue()
    vmm_top = vmm_wrapper(vmm_in_queue, vmm_out_queue)
    input_dict = Bundle()
    input_dict.Members['addr'] = IO()
    input_dict.Members['d_in'] = IO()
    input_dict.Members['sram_wr'] = IO()
    input_dict.Members['sram_rd'] = IO()
    result_dict = {}


    while True:
        await RisingEdge(clock)
        input_dict.Members['addr'].Data = dut.vmm.vmm.vmm_top.addr.value
        input_dict.Members['d_in'].Data = dut.vmm.vmm.vmm_top.d_in.value
        input_dict.Members['sram_wr'].Data = dut.vmm.vmm.vmm_top.sram_wr.value
        input_dict.Members['sram_rd'].Data = dut.vmm.vmm.vmm_top.sram_rd.value
        await vmm_in_queue.put(input_dict)
        result_dict = await vmm_out_queue.get()
        dut.vmm.vmm.vmm_top.adc_data_ready.value = result_dict['adc_data_ready'].Data
        dut.vmm.vmm.vmm_top.adc_0.value  = result_dict['adc_00'].Data
        dut.vmm.vmm.vmm_top.adc_1.value  = result_dict['adc_01'].Data
        dut.vmm.vmm.vmm_top.adc_2.value  = result_dict['adc_02'].Data
        dut.vmm.vmm.vmm_top.adc_3.value  = result_dict['adc_03'].Data
        dut.vmm.vmm.vmm_top.adc_4.value  = result_dict['adc_04'].Data
        dut.vmm.vmm.vmm_top.adc_5.value  = result_dict['adc_05'].Data
        dut.vmm.vmm.vmm_top.adc_6.value  = result_dict['adc_06'].Data
        dut.vmm.vmm.vmm_top.adc_7.value  = result_dict['adc_07'].Data
        dut.vmm.vmm.vmm_top.adc_8.value  = result_dict['adc_08'].Data
        dut.vmm.vmm.vmm_top.adc_9.value  = result_dict['adc_09'].Data
        dut.vmm.vmm.vmm_top.adc_10.value = result_dict['adc_10'].Data
        dut.vmm.vmm.vmm_top.adc_11.value = result_dict['adc_11'].Data
        dut.vmm.vmm.vmm_top.adc_12.value = result_dict['adc_12'].Data
        dut.vmm.vmm.vmm_top.adc_13.value = result_dict['adc_13'].Data
        dut.vmm.vmm.vmm_top.adc_14.value = result_dict['adc_14'].Data
        dut.vmm.vmm.vmm_top.adc_15.value = result_dict['adc_15'].Data
        dut.vmm.vmm.vmm_top.adc_16.value = result_dict['adc_16'].Data
        dut.vmm.vmm.vmm_top.adc_17.value = result_dict['adc_17'].Data
        dut.vmm.vmm.vmm_top.adc_18.value = result_dict['adc_18'].Data
        dut.vmm.vmm.vmm_top.adc_19.value = result_dict['adc_19'].Data
        dut.vmm.vmm.vmm_top.adc_20.value = result_dict['adc_20'].Data
        dut.vmm.vmm.vmm_top.adc_21.value = result_dict['adc_21'].Data
        dut.vmm.vmm.vmm_top.adc_22.value = result_dict['adc_22'].Data
        dut.vmm.vmm.vmm_top.adc_23.value = result_dict['adc_23'].Data
        dut.vmm.vmm.vmm_top.adc_24.value = result_dict['adc_24'].Data
        dut.vmm.vmm.vmm_top.adc_25.value = result_dict['adc_25'].Data
        dut.vmm.vmm.vmm_top.adc_26.value = result_dict['adc_26'].Data
        dut.vmm.vmm.vmm_top.adc_27.value = result_dict['adc_27'].Data
        dut.vmm.vmm.vmm_top.adc_28.value = result_dict['adc_28'].Data
        dut.vmm.vmm.vmm_top.adc_29.value = result_dict['adc_29'].Data
        dut.vmm.vmm.vmm_top.adc_30.value = result_dict['adc_30'].Data
        dut.vmm.vmm.vmm_top.adc_31.value = result_dict['adc_31'].Data
    
    
TIMEOUT = 1_000_000_000_000

# NOTE: icarus supports only 'step's as time unit, therefore we should use only them

# Timeout is 2x'd because a clock cycle is 2 steps
@cocotb.test(timeout_time=2*TIMEOUT, timeout_unit="step")
async def test_ACoreChip_with_VMM(dut):
    """
    This test loads the program from an elf file into ROM, toggles reset, and runs the clock
    for a specified number of clock cycles
    """

    async def jtag_cycle(tck, tms, tdi, trstn):
        """
        Execute one jtag cycle.
        """
        try:
            dut.io_jtag_TMS.value = tms
            dut.io_jtag_TDI.value = tdi
            dut.io_jtag_TCK.value = tck
            dut.io_jtag_TRSTn.value = trstn
            await ClockCycles(clock, 1)
        except Exception as e:
            print(e)

    elf_path = cocotb.plusargs["elf"]
    selfchecking = cocotb.plusargs["selfchecking"] == "True"
    jtag_file = cocotb.plusargs["jtag_file"]
    if not elf_path:
        raise Exception("Please provide an elf file. Example: make ELF_FILE=/path/to/elf.elf")
    elf_file = common.read_elf(elf_path)

    clock = dut.clock
    reset = dut.reset

    # Dummy values to input IOs
    dut.io_gpi.value = 0x55

    cocotb.start_soon(Clock(clock, 2, units="step").start())
    await jtag_cycle(0, 0, 0, 0)

    jtag_array = np.loadtxt(jtag_file)

    
    # Initialize ROM for riscv-tests
    rom_blob = common.rom_image(elf_file)
    elf_file.close()
    rom_blocks = [
        dut.progmem.mem_array_0,
        dut.progmem.mem_array_1,
        dut.progmem.mem_array_2,
        dut.progmem.mem_array_3
        ]
    for i in range(len(rom_blob)):
        rom_blocks[i%len(rom_blocks)][i//len(rom_blocks)].value = int(rom_blob[i])

    reset.value = 0
    await ClockCycles(clock, 2)
    reset.value = 1
    await ClockCycles(clock, 2)
    reset.value = 0
    await ClockCycles(clock, 2)

    for item in jtag_array:
        await jtag_cycle(int(item[0]), int(item[1]), int(item[2]), int(item[3]))

    cocotb.start_soon(connect_vmm(dut, clock))

    if selfchecking:
        await Edge(dut.core.csreg_block.csregs.mstopsim)
        assert dut.core.regfile_block.regfile.x_10 == 1
    else:
        await ClockCycles(clock, 50000)
