import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

import socket
from thesdk import *
from acore_methods import *
import acore_methods.pathutils as pathutils
from .testbench import TestBench
from ACoreChip import *
from ACoreChip.controller import jtag_controller
from JTAG.driver import jtag_driver
from jtag_programmer import *

class ZC706Testbench(TestBench):

#region properties
#endregion properties

    def __init__(self, **kwargs):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__))
        self.Rs = 100e6
        self.init(**kwargs)

    def init(self, **kwargs):
        """
        Generate ZC706 programming sequence.
        NOTE: Currently expects that FPGA synth+impl+prog is done manually elsewhere
        """

        acore_methods.init_props_from_kwargs(self, **kwargs)

        # paths to locations used by ACoreTests build system
        self._hw_config_name = pathutils.barename(self.test_config['hw_config_yaml'])
        self._acoretests_build_dir = os.path.realpath(os.path.join(self.entitypath, '../ACoreTests/build/'))
        self._test_config_build_dir = os.path.join(self._acoretests_build_dir, 'tests', 'programs', self.test_target, self._hw_config_name)

        # parse keyword arguments
        self.program_file = kwargs.get("program_file")
        self.jtag_config_file = kwargs.get("jtag_config_file")
        self.isa_string = kwargs.get("isa_string", "rv32imf")

        # setup dut
        self.dut = ACoreChip()
        self.fd = open(self.test_config['hw_config']['jtag_config_yaml'], 'r')
        self.jtag_config = yaml.load(self.fd, yaml.Loader)
        self.fd.close()
        self.dut.vlogext = '.v'

        self.dut.model = 'sv'
        self.dut.Rs = self.Rs
        self.dut.preserve_iofiles = False
        self.dut.interactive_rtl = True

        self._driver = jtag_driver()
        self.namemap = {
            'address':  'prog_iface_addr',
            'data':     'prog_iface_data',
            'write_en': 'prog_iface_write_en'
        }

        self.p = jtag_programmer(
            driver=self._driver, config=self.jtag_config, namemap=self.namemap)

        # control sequencing
        self.jtag_ctrl = jtag_controller(self._driver, self.jtag_config)

    def run(self):
        '''
        Upload JTAG programming sequence over TCP/IP using remote_bitbang protocol.
        '''

        # generate JTAG control sequence
        self._driver.reset_tap()
        self._driver.tms_reset()
        self.jtag_ctrl.set_core_en(0)
        self.p.read_elf(os.path.join(self._test_config_build_dir, 'bin', 'fpga.elf'))
        self.p.run()
        self.jtag_ctrl.set_core_en(1)

        # ad hoc driver
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP socket
        #ip = '130.233.167.214' #fpga4
        ip = '130.233.167.197' #fpga6
        port = 7
        self.print_log(type='I', msg='Connecting to %s:%s' % (ip, port))
        sock.connect((ip, port))
        #import progressbar
        #for sample in progressbar.progressbar(sample_commands):
        self.print_log(type='I', msg='Sending JTAG remote_bitbang data')
        sock.sendall(self._driver.get_remote_bitbang())
        self.print_log(type='I', msg='Done sending JTAG remote_bitbang data')

        # At this point, you should import results to this Entity for postprocessing
        # and run the postprocessors, whatever they are.