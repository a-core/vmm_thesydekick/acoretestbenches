# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, Edge, RisingEdge
import common

TIMEOUT = 10000

@cocotb.test(timeout_time=2*TIMEOUT, timeout_unit="step")
async def test_ACoreChip(dut):
    """
    This test loads the program from an elf file into program memory, toggles reset, and runs the clock
    for a specified number of clock cycles
    """
    elf_path = cocotb.plusargs["elf"]
    if not elf_path:
        raise Exception("Please provide an elf file. Example: make ELF_FILE=/path/to/elf.elf")

    elf_file = common.read_elf(elf_path)

    clock = dut.clock
    reset = dut.reset

    cocotb.start_soon(Clock(clock, 2, units="step").start())

    # Initialize ROM for riscv-tests
    rom_blob = common.rom_image(elf_file)
    rom_blocks = [
        dut.progmem.mem_array_0,
        dut.progmem.mem_array_1,
        dut.progmem.mem_array_2,
        dut.progmem.mem_array_3
        ]
    for i in range(len(rom_blob)):
        rom_blocks[i%4][i//4].value = int(rom_blob[i])

    # Initialize RAM for riscv-tests
    ram_blob = common.ram_image(elf_file)
    ram_blocks = [
        dut.ram.mem_array_0,
        dut.ram.mem_array_1,
        dut.ram.mem_array_2,
        dut.ram.mem_array_3
        ]
    for i in range(len(ram_blob)):
        ram_blocks[i%4][i//4].value = int(ram_blob[i])

    reset.value = 0
    dut.core_io_core_en.value = 0
    await ClockCycles(clock, 2)
    reset.value = 1
    await ClockCycles(clock, 2)
    dut.core_io_core_en.value = 1
    reset.value = 0

    await Edge(dut.core.csreg_block.csregs.mstopsim)
    assert dut.core.regfile_block.regfile.x_10 == 1
