import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from acore_methods import *
import acore_methods.pathutils as pathutils
from ACoreChip import *
from ACoreChip.controller import controller as ACoreChip_controller
from ACoreChip.controller import jtag_controller
from JTAG.driver import jtag_driver
from jtag_programmer import *
from .testbench import TestBench
import pathlib
import cocotb_test.simulator
import shutil
import tempfile
import numpy as np
import logging


class SimTestbench(TestBench):

#region properties

    @property
    def simulator(self):
        """
        Simulation framework.

        Options: ``'thesdk' | 'cocotb'``
        """
        if not hasattr(self, '_simulator'):
            self._simulator = None
        return self._simulator
    @simulator.setter
    def simulator(self, value):
        if value in ['thesdk', 'cocotb']:
            self._simulator = value
        else:
            raise ValueError("invalid value: %s" % (value))

    @property
    def sim_backend(self):
        """
        Simulator backend.

        Options: ``'questa' | 'icarus' | 'verilator'``
        """
        if not hasattr(self, '_backend'):
            self._backend = None
        return self._backend
    @sim_backend.setter
    def sim_backend(self, value):
        if value in ['questa', 'icarus', 'verilator']:
            self._backend = value
        else:
            raise ValueError("invalid value: %s" % (value))

    @property
    def interactive_sim(self):
        """
        Toggles whether thesdk flow opens waveform viewer after simulation.
        Toggles whether Cocotb generates a VCD file.
        
        Options: ``True | False``
        """
        if not hasattr(self, '_interactive_sim'):
            self._interactive_sim = None
        return self._interactive_sim
    @interactive_sim.setter
    def interactive_sim(self, value):
        if type(value) is bool:
            self._interactive_sim = value
        else:
            raise ValueError("invalid value: %s" % (value))

    @property
    def interactive_controlfile(self):
        """
        Path to simulator command file.
        """
        if not hasattr(self, '_interactive_controlfile'):
            self._interactive_controlfile = None
        return self._interactive_controlfile
    @interactive_controlfile.setter
    def interactive_controlfile(self, value):
        self._interactive_controlfile = value

    @property
    def questa_wave_fmt(self):
        """
        Format in which Questa Simulator exports a waveform. Supported by riscv-tests.

        Options: ``'wlf' | 'vcd' | 'none'``
        """
        if not hasattr(self, '_questa_wave_fmt'):
            self._questa_wave_fmt = None
        return self._questa_wave_fmt
    @questa_wave_fmt.setter
    def questa_wave_fmt(self, value):
        if value in ['wlf', 'vcd', 'none']:
            self._questa_wave_fmt = value
        else:
            raise ValueError("invalid value: %s" % (value))

    @property
    def cocotb_wave_fmt(self):
        """
        Format in which the cocotb flow outputs a waveform.
        VCD supported by all simulators, FST for verilator and icarus

        Options: ``'vcd' | 'fst'``
        """
        if not hasattr(self, '_cocotb_wave_fmt'):
            self._cocotb_wave_fmt = None
        return self._cocotb_wave_fmt
    @cocotb_wave_fmt.setter
    def cocotb_wave_fmt(self, value):
        if value in ['vcd', 'fst']:
            self._cocotb_wave_fmt = value
        else:
            raise ValueError("invalid value: %s" % (value))

#endregion properties

    def __init__(self, **kwargs):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__))
        self.Rs = 100e6
        self.init(**kwargs)

    def init(self, **kwargs):
        """
        Initializes testbench for generic simulations.

        Generic simulation configurations are simple combinations of:
        - set of enabled isa extensions
        - ELF file containing software executable
        - other processor configuration (e.g. JTAG configuration)

        Note that because of the Makefile interface we are using,
        the default values for items in ``kwargs`` come from the Makefile.

        Properties are automatically initialized from ``kwargs`` when the
        keyword matches the property name.
        """
        acore_methods.init_props_from_kwargs(self, **kwargs)

        self._hw_config_name = pathutils.barename(self.test_config['hw_config_yaml'])
        self._acoretests_build_dir = os.path.realpath(os.path.join(self.entitypath, '../ACoreTests/build/'))
        self._test_config_build_dir = os.path.join(self._acoretests_build_dir, 'tests', 'programs', self.test_target, self._hw_config_name)

        # setup dut
        self.dut = ACoreChip()

        # Symlink verilog files from 'build/chisel/' to rtl simulation directory
        verilog_path = os.path.join(self._acoretests_build_dir, 'chisel', self._hw_config_name)
        verilog_sources = glob.glob(os.path.join(verilog_path, '*.v'))
        for vsrc in verilog_sources:
            dstfile = os.path.join(self.dut.rtlsimpath, os.path.basename(vsrc))
            shutil.copyfile(vsrc, dstfile)

        self.dut.vlogext = '.v'
        self.dut.vlogmodulefiles = ['AsyncResetSyncBB.v', 'comp_amp.v', 'control_wrapper.v', 'vmm_top.v']

        if self.interactive_controlfile is not None:
            self.dut.interactive_controlfile = self.interactive_controlfile

        # set thesdk model based on simulation backend
        if self.sim_backend == "questa":
            self.dut.model = 'sv'
        elif self.sim_backend == "icarus":
            self.dut.model = 'icarus'
        elif self.sim_backend == 'verilator' and self.simulator == 'cocotb':
            pass
        else:
            self.print_log(type='F', msg=('Unsupported backend: ' + self.sim_backend))

        self.dut.Rs = self.Rs
        self.dut.preserve_rtlfiles = False
        self.dut.preserve_iofiles = False
        self.dut.interactive_rtl = self.interactive_sim

        # Saves all signals to a VCD file
        if not self.interactive_sim:
            self.dut.interactive_controlfile = \
                os.path.join(self.dut.entitypath, "interactive_control_files/modelsim/log_all_to_{}.do".format(self.questa_wave_fmt))

        self.driver = jtag_driver()
        self.namemap = {
            'address':  'prog_iface_addr',
            'data':     'prog_iface_data',
            'write_en': 'prog_iface_write_en'
        }
        self.p = jtag_programmer(
            driver=self.driver, config=self.test_config['hw_config']['jtag_config'], namemap=self.namemap)

        self.jtag_ctrl = jtag_controller(self.driver, self.test_config['hw_config']['jtag_config'])

        if self.simulator == 'thesdk':
            # simulation sequencing
            self.controller = ACoreChip_controller(dut=self.dut)
            self.controller.Rs = self.Rs  # This can be propagatet with proplist attribute


            # Connect IOs
            self.dut.IOS.Members['control_write'] = self.controller.IOS.Members['control_write']
            self.mstopsim_init()
        elif self.simulator == 'cocotb':
            pass
    
    def cocotbify(self):
        # "cocotbify" the verilog - allows generating VCD trace of the simulation
        cocotbify_cmd = 'cocotbify -v ' + self.dut.simdut + ' -o ' + self.dut.simdut
        self.print_log(type='I', msg="Running command: %s" % cocotbify_cmd)
        try:
            os.system(cocotbify_cmd)
        except Exception as e:
            self.print_log(type='I', msg="Already cocotbifyied.")

    def run(self):
        '''
        Run this testbench.
        '''
        self.driver.reset_tap()
        self.jtag_ctrl.set_core_en(0)

        if self.simulator == 'thesdk':
            self.p.read_elf(os.path.join(self._test_config_build_dir, 'bin', 'sim.elf'))
            self.p.run()
            self.jtag_ctrl.set_core_en(1)

            # Get IO excitations from JTAG driver
            self.dut.IOS.Members['jtag_tap_in'].Data = self.driver.get_numpy_array()

            self.controller.reset()
            self.controller.step_time()

            self.controller.start_datafeed()
            self.controller.step_time(step=len(self.dut.IOS.Members['jtag_tap_in'].Data)*self.controller.step)
            self.controller.step_time(step=50000*self.controller.step)
            self.controller.set_simdone()

            self.dut.init()
            self.dut.run()

            if self.test_config['sim']['selfchecking']:
                self.check_sim_result()

        elif self.simulator == 'cocotb':
            # TODO Don't hardcode 'bin' it's already defined somewhere
            dir_out = os.path.join(self._test_config_build_dir, 'results')
            if not os.path.exists(dir_out):
                os.mkdir(dir_out)
            sim_out = os.path.join(dir_out, 'sim_out')

            # Set up logging to both stdout and file
            # TODO How to choose logging to file from make interface?
            cocotb_logger = cocotb_test.simulator.logging.getLogger()

            file_handler = logging.StreamHandler(sys.stdout)
            # file_handler.setLevel(logging.WARN) # Remove this line for more info on stdout
            cocotb_logger.addHandler(file_handler)

            file_handler = logging.FileHandler(sim_out, "w")
            file_handler.setLevel(logging.ERROR)
            cocotb_logger.addHandler(file_handler)

            self.jtag_ctrl.set_core_en(1)
            # Using icarus (iverilog) as it is better supported
            os.environ["SIM"] = self.sim_backend
            # Set current directory as work directory
            os.chdir(pathlib.Path(__file__).parent)
            # Collect all verilog sources
            verilog_sources = [self.dut.simdut] + \
                [os.path.join(self.dut.rtlsimpath, file) for file in self.dut.vlogmodulefiles]
            # Run cocotb simulation
            jtag_file = tempfile.NamedTemporaryFile()
            np.savetxt(jtag_file.name, self.driver.get_numpy_array())
            try:
                compile_args = []
                sim_args = []
                waves = False
                dump_target = ""

                if self.sim_backend == 'icarus':
                    if self.interactive_sim:
                        if self.cocotb_wave_fmt == 'fst':
                            waves = True
                            dump_target = "iverilog_dump.fst"
                        elif self.cocotb_wave_fmt == 'vcd':
                            self.cocotbify()
                            dump_target = "ACoreChip.vcd"
                elif self.sim_backend == 'verilator':
                    # Waive warnings
                    compile_args += ['-Wno-WIDTHEXPAND'] + ['-Wno-ASCRANGE']
                    if self.interactive_sim:
                        if self.cocotb_wave_fmt == 'fst':
                            waves = True
                            dump_target = "dump.fst"
                        elif self.cocotb_wave_fmt == 'vcd':
                            compile_args += ['--trace']
                            dump_target = "dump.vcd"
                elif self.sim_backend == 'questa':
                    # Questa modifies LD_LIBRARY_PATH internally if not set
                    # This makes it use a very old gcc that is not compatible with
                    # modern cocotb or numpy
                    sim_args += ['-noautoldlibpath']
                    if self.interactive_sim:
                        waves = False
                        self.print_log(type='W', msg="Questa in cocotb flow does not support wave dumps for now.")
                cocotb_test.simulator.run(
                    verilog_sources=verilog_sources,
                    work_dir=os.getcwd(),
                    toplevel="ACoreChip",
                    module="test_acore_vmm",
                    compile_args=compile_args,
                    sim_args=sim_args,
                    waves=waves,
                    plus_args=["+jtag_file=" + jtag_file.name,
                                "+elf=" + os.path.join(self._test_config_build_dir, 'bin', 'sim.elf'),
                                "+selfchecking=" + str(self.test_config['sim']['selfchecking'])]
                )
                self.print_log(type='I', msg="Test passed!")
            except AssertionError:
                self.print_log(type='W', msg="Test failed!")

            if self.interactive_sim:
                if self.cocotb_wave_fmt == 'fst':
                    self.print_log(type='I', msg="Dumping FST.")
                    if self.sim_backend == 'icarus':
                        os.rename("ACoreChip.fst", "../../ACoreTests/ACoreTests/ACoreChip_dump.fst")
                    elif self.sim_backend == 'verilator':
                        os.rename("dump.fst", "../../ACoreTests/ACoreTests/ACoreChip_dump.fst")
                elif self.cocotb_wave_fmt == 'vcd':
                    self.print_log(type='I', msg="Dumping VCD.")
                    if self.sim_backend == 'icarus':
                        os.rename("ACoreChip.vcd", "../../ACoreTests/ACoreTests/ACoreChip_dump.vcd")
                    elif self.sim_backend == 'verilator':
                        os.rename("dump.vcd", "../../ACoreTests/ACoreTests/ACoreChip_dump.vcd")
        else:
            self.print_log(type='F', msg="Unsupported simulator! Supported are 'thesdk' and 'cocotb'")

    def check_sim_result(self):
        try:
            # Take the last value
            result = self.dut.IOS.Members['a0'].Data[-1]
        except TypeError: # Raised if there is no data in a0
            self.print_log(type='W', msg="Test failed! There was no data in a0.")

        if result != '1':
            self.print_log(type='W', msg="Test failed! a0 was not 1.")
        else:
            self.print_log(type='I', msg="Test passed!")


    def mstopsim_init(self):
        # Connectors to store a0 (x10) to an iofile (the iofile is defined under run-command)
        # Also generates mstopsim wire that connects to mstopsim CSR
        self.dut.custom_connectors = verilog_connector_bundle()
        self.dut.custom_connectors.Members['a0'] = verilog_connector(name='a0', cls='wire')
        self.dut.custom_connectors.Members['mstopsim'] = verilog_connector(name='mstopsim', cls='wire')
        self.dut.custom_connectors.Members['ACoreChip.core.csreg_block.csregs.mstopsim'] = verilog_connector(name='ACoreChip.core.csreg_block.csregs.mstopsim')
        self.dut.custom_connectors.Members['ACoreChip.core.regfile_block.regfile.x_10'] = verilog_connector(name='ACoreChip.core.regfile_block.regfile.x_10')
        self.dut.custom_connectors.Members['mstopsim'].connect = self.dut.custom_connectors.Members['ACoreChip.core.csreg_block.csregs.mstopsim']
        self.dut.custom_connectors.Members['a0'].connect = self.dut.custom_connectors.Members['ACoreChip.core.regfile_block.regfile.x_10']
        self.dut.assignment_matchlist = ['mstopsim', 'a0']


        # Makes simulation stop when test program writes in mstopsim register
        stop_early_cmd = """
reg mstopsim_delayed;
always @(posedge clock) begin
    mstopsim_delayed <= mstopsim;
end
always @(mstopsim_delayed) begin
    if (mstopsim_delayed == 1) begin
        $finish;
    end
end
            """
        self.dut.rtlmisc = [stop_early_cmd]

        # Saves a0 to an iofile when mstopsim is 1
        self.dut.IOS.Members['a0'] = IO()
        a0 = rtl_iofile(self.dut, name='a0', dir='out', iotype='sample', ionames=['a0'], datatype='sint')
        a0.verilog_io_condition = 'mstopsim == 1'
