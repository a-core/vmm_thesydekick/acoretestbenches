# common methods reused by multiple cocotb testbenches

from elftools.elf.elffile import ELFFile
import numpy as np

def read_elf(elf_path):
    fd = open(elf_path, 'rb')
    return ELFFile(fd)

def get_symbol_value(elf_file, name):
    symtab = elf_file.get_section_by_name('.symtab')
    symbol = symtab.get_symbol_by_name(name)[0]
    return symbol['st_value']

def rom_start(elf_file):
    return get_symbol_value(elf_file, '__TEXT_BEGIN__')

def ram_start(elf_file):
    return get_symbol_value(elf_file, '__DATA_BEGIN__')

def bss_size(elf_file):
    bss_start = get_symbol_value(elf_file, '__bss_start')
    end = get_symbol_value(elf_file, '_end')
    return end - bss_start

def rom_image(elf_file):
    # ROM image contents as a byte string
    sections = ['.text', '.data', '.sdata', '.rodata']
    rom_bytes = b''
    for sect_name in sections:
        section = elf_file.get_section_by_name(sect_name)
        if section is not None:
            rom_bytes += section.data()
    return np.array([b for b in rom_bytes]).astype(int)

def ram_image(elf_file):
    # data section contents
    sections = ['.data', '.sdata']
    ram_bytes = b''
    for sect_name in sections:
        section = elf_file.get_section_by_name(sect_name)
        if section is not None:
            ram_bytes += section.data()
    # append bss blob
    ram_bytes += bss_size(elf_file)*b''
    return np.array([b for b in ram_bytes]).astype(int)
